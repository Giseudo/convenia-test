# convenia-test

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and hot-reloads for Storybook development
```
yarn run serve:storybook
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```
