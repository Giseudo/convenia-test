import Vue from 'vue'
import Vuex from 'vuex'
import product from './storeModules/product'
import order from './storeModules/order'
import payment from './storeModules/payment'
import table from './storeModules/table'

Vue.use(Vuex)

// Create Modules object
const modules = {
	product,
	table,
	payment,
	order
}

// Dynamically import and namespace Vuex modules
const moduleNames = []

moduleNames.forEach(name => {
  let module = require(`./storeModules/${name}`)

  modules[name] = {
    namespaced: true,
    ...module.default
  }
})

const store = new Vuex.Store({
	modules,

	actions: {
		init({ dispatch }, data) {
			return new Promise((resolve, reject) => {
				Promise.all([
					dispatch('product/init', data.products),
					dispatch('table/init', data.tables)
				])
					.then((context) => {
						return resolve(context)
					})
			})
		}
	}
})

export default store
