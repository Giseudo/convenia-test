import { storiesOf } from '@storybook/vue'
import CNButtonIcon from './index'

storiesOf('Atom - cn-button-icon', module)
  .add('primary', () => ({
    components: {
      'cn-button-icon': CNButtonIcon
    },
    data: () => ({
      style: {
        container: {
          margin: '24px',
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center'
        },
        button: {
        }
      }
    }),
    template: `
			<div :style="style.container">
				<cn-button-icon theme="primary" size="sm" :style="style.button" />
				<cn-button-icon theme="primary" size="md" :style="style.button" />
        <cn-button-icon theme="primary" size="lg" :style="style.button" />
			</div>
		`
  }))

  .add('accent', () => ({
    components: {
      'cn-button-icon': CNButtonIcon
    },
    data: () => ({
      style: {
        container: {
          margin: '24px',
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center'
        },
        button: {
        }
      }
    }),
    template: `
			<div :style="style.container">
				<cn-button-icon theme="accent" size="sm" :style="style.button" />
				<cn-button-icon theme="accent" size="md" :style="style.button" />
        <cn-button-icon theme="accent" size="lg" :style="style.button" />
			</div>
		`
  }))

  .add('warn', () => ({
    components: {
      'cn-button-icon': CNButtonIcon
    },
    data: () => ({
      style: {
        container: {
          margin: '24px',
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center'
        },
        button: {
        }
      }
    }),
    template: `
			<div :style="style.container">
				<cn-button-icon theme="warn" size="sm" :style="style.button" />
				<cn-button-icon theme="warn" size="md" :style="style.button" />
        <cn-button-icon theme="warn" size="lg" :style="style.button" />
			</div>
		`
  }))
