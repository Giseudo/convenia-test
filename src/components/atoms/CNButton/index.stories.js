import { storiesOf } from '@storybook/vue'
import CNButton from './index'

storiesOf('Atom - cn-button', module)
  .add('primary', () => ({
    components: {
      'cn-button': CNButton
    },
    data: () => ({
      style: {
        container: {
          margin: '24px',
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center'
        },
        button: {
          marginRight: '8px'
        }
      }
    }),
    template: `
			<div :style="style.container">
				<cn-button theme="primary" size="sm" :style="style.button">
					Small
				</cn-button>
				<cn-button theme="primary" size="md" :style="style.button">
					Medium
				</cn-button>
        <cn-button theme="primary" size="lg" :style="style.button">
					Large
				</cn-button>
			</div>
		`
  }))

  .add('accent', () => ({
    components: {
      'cn-button': CNButton
    },
    data: () => ({
      style: {
        container: {
          margin: '24px',
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center'
        },
        button: {
          marginRight: '8px'
        }
      }
    }),
    template: `
			<div :style="style.container">
				<cn-button theme="accent" size="sm" :style="style.button">
					Small
				</cn-button>
				<cn-button theme="accent" size="md" :style="style.button">
					Medium
				</cn-button>
        <cn-button theme="accent" size="lg" :style="style.button">
					Large
				</cn-button>
			</div>
		`
  }))

  .add('warn', () => ({
    components: {
      'cn-button': CNButton
    },
    data: () => ({
      style: {
        container: {
          margin: '24px',
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center'
        },
        button: {
          marginRight: '8px'
        }
      }
    }),
    template: `
			<div :style="style.container">
				<cn-button theme="warn" size="sm" :style="style.button">
					Small
				</cn-button>
				<cn-button theme="warn" size="md" :style="style.button">
					Medium
				</cn-button>
        <cn-button theme="warn" size="lg" :style="style.button">
					Large
				</cn-button>
			</div>
		`
  }))
