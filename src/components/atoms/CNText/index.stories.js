import { storiesOf } from '@storybook/vue'
import CNText from './index'

storiesOf('Atom - cn-text', module)
  .add('default', () => ({
    components: {
      'cn-text': CNText
    },
    data: () => ({
      style: {
        container: {
          margin: '24px',
          background: 'white',
          padding: '24px',
          display: 'flex',
          flexFlow: 'column'
        }
      }
    }),
    template: `
			<div :style="style.container">
				<cn-text type="title">
					Title
				</cn-text>
				<cn-text type="subhead">
					Subhead
				</cn-text>
				<cn-text type="body">
					Body
				</cn-text>
        <cn-text type="lead">
					Lead
				</cn-text>
        <cn-text type="caption">
					Caption
				</cn-text>				
        <cn-text type="fixed">
					Fixed
				</cn-text>
			</div>
		`
  }))

  .add('dark', () => ({
    components: {
      'cn-text': CNText
    },
    data: () => ({
      style: {
        container: {
          margin: '25px',
          background: 'black',
          padding: '24px',
          display: 'flex',
          flexFlow: 'column'
        }
      }
    }),
    template: `
			<div :style="style.container">
				<cn-text type="title" :dark="true">
					Title
				</cn-text>
				<cn-text type="subhead" :dark="true">
					Subhead
				</cn-text>
				<cn-text type="body" :dark="true">
					Body
				</cn-text>
        <cn-text type="lead" :dark="true">
					Lead
				</cn-text>
        <cn-text type="caption" :dark="true">
					Caption
				</cn-text>				
        <cn-text type="fixed" :dark="true">
					Fixed
				</cn-text>
			</div>
		`
  }))
