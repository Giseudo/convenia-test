import { storiesOf } from '@storybook/vue'
import CNText from '@/components/atoms/CNText'
import CNModal from './index'

storiesOf('Atom - cn-modal', module)
  .add('primary', () => ({
    components: {
      'cn-text': CNText,
      'cn-modal': CNModal
    },
    data: () => ({
      style: {}
    }),
    template: `
			<cn-modal>
        <cn-text type="title">
          Hello world! :)
        </cn-text>
			</cn-modal>
		`
  }))