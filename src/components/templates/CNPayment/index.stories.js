import { storiesOf } from '@storybook/vue'
import CNPayment from './index'

storiesOf('Template - cn-payment', module)
  .add('default', () => ({
    components: {
      'cn-payment': CNPayment
    },
    data: () => ({
      style: {}
    }),
    template: `
      <cn-payment />
		`
  }))
