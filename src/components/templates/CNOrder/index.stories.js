import { storiesOf } from '@storybook/vue'
import CNOrder from './index'

storiesOf('Template - cn-order', module)
  .add('default', () => ({
    components: {
      'cn-order': CNOrder
    },
    data: () => ({
      style: {}
    }),
    template: `
      <cn-order />
		`
  }))