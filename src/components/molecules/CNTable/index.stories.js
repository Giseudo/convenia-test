import { storiesOf } from '@storybook/vue'
import CNTable from './index'

storiesOf('Molecule - cn-table', module)
  .add('default', () => ({
    components: {
      'cn-table': CNTable
    },
    data: () => ({
      style: {
        container: {
          margin: '24px',
          display: 'flex',
          justifyContent: 'space-around',
          alignItems: 'center'
        },
        table: {
        }
      }
    }),
    template: `
			<div :style="style.container">
				<cn-table :style="style.table" />
				<cn-table :style="style.table" :opened="new Date()" />
			</div>
		`
  }))
