import { storiesOf } from '@storybook/vue'
import CNToolbar from './index'

storiesOf('Molecule - cn-toolbar', module)
  .add('primary', () => ({
    components: {
      'cn-toolbar': CNToolbar
    },
    data: () => ({
      style: {},
    }),
    template: `
			<cn-toolbar />
		`
  }))
