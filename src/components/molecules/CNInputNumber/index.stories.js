import { storiesOf } from '@storybook/vue'
import CNInputNumber from './index'

storiesOf('Molecule - cn-input-number', module)
  .add('primary', () => ({
    components: {
      'cn-input-number': CNInputNumber
    },
    data: () => ({
      style: {},
			price: 2300
    }),
    template: `
			<cn-input-number
				label="R$"
				:value="price"
				:max="5000000"
			/>
		`
  }))
