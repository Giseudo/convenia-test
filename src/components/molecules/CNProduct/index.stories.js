import { storiesOf } from '@storybook/vue'
import CNProduct from './index'

storiesOf('Molecule - cn-product', module)
  .add('default', () => ({
    components: {
      'cn-product': CNProduct
    },
    data: () => ({
      amount: 3,
      style: {
        container: {
          margin: '24px',
        }
      }
    }),
    template: `
			<div :style="style.container">
        <cn-product :amount.sync="amount" />
			</div>
		`
  }))