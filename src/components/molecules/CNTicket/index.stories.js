import { storiesOf } from '@storybook/vue'
import CNTicket from './index'

storiesOf('Molecule - cn-ticket', module)
  .add('default', () => ({
    components: {
      'cn-ticket': CNTicket
    },
    data: () => ({
      style: {
				margin: '24px',
      }
    }),
    template: `
			<cn-ticket :style="style" />
		`
  }))
