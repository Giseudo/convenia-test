import { storiesOf } from '@storybook/vue'
import CNText from '@/components/atoms/CNText'
import CNDialog from './index'

storiesOf('Molecule - cn-dialog', module)
  .add('primary', () => ({
    components: {
      'cn-text': CNText,
      'cn-dialog': CNDialog
    },
    data: () => ({
      style: {}
    }),
    template: `
			<cn-dialog>
        <cn-text type="lead">
          Mauris mollis sapien nibh, sit amet venenatis ipsum facilisis a. Etiam egestas tempor ante, eu gravida odio porttitor id. Aliquam et odio id quam sollicitudin maximus non vel libero. Praesent odio nunc, finibus nec lobortis vitae, rutrum vel enim. Fusce in auctor augue.
        </cn-text>
			</cn-dialog>
		`
  }))