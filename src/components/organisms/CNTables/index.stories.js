import { storiesOf } from '@storybook/vue'
import CNTables from './index'

storiesOf('Organism - cn-tables', module)
  .add('default', () => ({
    components: {
      'cn-tables': CNTables
    },
    data: () => ({
      style: {
        container: {
        },
      }
    }),
    template: `
      <cn-tables />
		`
  }))