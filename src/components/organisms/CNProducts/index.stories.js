import { storiesOf } from '@storybook/vue'
import CNProducts from './index'

storiesOf('Organism - cn-products', module)
  .add('default', () => ({
    components: {
      'cn-products': CNProducts
    },
    data: () => ({
      style: {
        container: {
        }
      },
      selected: {}
    }),
    template: `
      <cn-products :closed="true" v-model="selected" />
		`
  }))