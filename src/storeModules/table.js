import Vue from 'vue'

export default {
	namespaced: true,

	state: {
		selected: null,
		tables: []
	},

	getters: {
		getSelected(state) {
			return state.selected
		},

		getTables(state) {
			return state.tables
		},

		// Find table with id
		find: (state) =>
			id => state.tables.find(
				table => table.id == id
			)
	},

	mutations: {
		// Set selected table
		select(state, table) {
			state.selected = table
		},

		// Add a new table
		add(state, table) {
			state.tables.push({
				// Default data
				id: new Date().getTime(),
				name: '',
				orders: [],
				payments: [],

				// Original data
				...table
			})
		},
	},

	actions: {
		// Set initial data
		init({ state, commit }, tables) {
			// Reset data
			state.tables = []

			return new Promise((resolve, reject) => {
				// Add tables
				tables.forEach(table =>
					commit('add', table)
				)

				// Return tables
				return resolve(state.tables)
			})
		},

		// Purchase products
		purchase({ state, dispatch }, orders) {
			return new Promise((resolve, reject) => {
				// Verify if table is selected
				if (!state.selected)
					reject(new Error("There's no table selected."))

				// Create orders
				dispatch('order/addOrders', orders, { root: true })
					.then(orders => {
						// Return if no products was selected
						if (orders.length == 0)
							return resolve([])

						// Open table if this is the first order
						if (state.selected)
							state.selected.opened = new Date()

						// Sum the total
						orders.forEach(order => {
							state.selected.debit += order.total
						})

						// Add orders to the table
						state.selected.orders = state.selected.orders.concat(orders)

						// Return added orders
						return resolve(state.selected)
					})
			})
		},

		pay({ state, dispatch }, value) {
			return new Promise((resolve, reject) => {
				// Verify if table is selected
				if (!state.selected)
					reject(new Error("There's no table selected."))

				// Create payment
				dispatch('payment/addPayment', value, { root: true })
					.then(payment => {
						// Add payment to the table
						state.selected.payments.push(payment)

						// Reduce value from table's debit
						state.selected.debit -= payment.value

						// Closes table if everything was paid
						if (state.selected.debit <= 0) {
							state.selected.debit = 0
							state.selected.opened = null
						}

						return resolve(state.selected)
					})
			})
		},
	}
}
