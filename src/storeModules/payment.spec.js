import Vue from 'vue'
import Vuex from 'vuex'
import { assert, expect } from 'chai'

import data from './data-mock'
import payment from './payment'
import table from './table'

Vue.use(Vuex)

const store = new Vuex.Store({ modules: { payment } })
const state = store.state.payment

describe('payments', () => {
	it('should load data', () => {
		return store.dispatch('payment/init', data.payments)
			.then(payments => {
				expect(state.payments.length)
					.to.equal(data.payments.length)
			})
	})

	it('should add payment', () =>
		store.dispatch('payment/addPayment', {
			id: 12345,
			table: 1,
			value: 15.0
		})
		.then(payment => {
			expect(state.payments.length)
				.to.equal(data.payments.length + 1)
		})
	)

	it('should find the payment', () =>
		store.dispatch('payment/getPayment', 12345)
		.then(payment => {
			assert.isObject(payment)
		})
	)

	it('should generate a random id', () =>
		store.dispatch('payment/addPayment', {
			table: 1,
			value: 20.0,
		})
		.then(payment => {
			assert.isNotNull(payment.id)
		})
	)
})
