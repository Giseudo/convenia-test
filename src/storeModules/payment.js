import store from '@/store'

export default {
	namespaced: true,

	state: {
		payments: []
	},

	getters: {
		// Retrieves payments
		getPayments(state) {
			return state.payments
		},

		// Find payment with id
		find: (state) =>
			id => state.payments.find(
				payment => payment.id == id
			)
	},

	mutations: {
		//
	},

	actions: {
		// Add a new product
		addPayment({ state, dispatch }, value) {
			return new Promise((resolve, reject) => {
				// Update state
				let payment = {
					id: new Date().getTime(),
					value: value
				}

				// Add payment to the state
				state.payments.push(payment)

				// Return payment
				return resolve(payment)
			})
		},
	}
}
