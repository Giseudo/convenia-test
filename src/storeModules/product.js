export default {
	namespaced: true,

	state: {
		products: [],
	},

	getters: {
		// Retrieve products
		getProducts(state) {
			return state.products
		},

		// Find product with id
		find: (state) =>
			id => state.products.find(
				product => product.id == id
			)
	},

	mutations: {
		//
	},

	actions: {
		// Initialize module
		init({ dispatch, state }, products) {
			// Reset data
			state.products = []

			return new Promise((resolve, reject) => {
				// Add initial products
				products.forEach(product => 
					dispatch('addProduct', product)
				)

				// Return products
				return resolve(state.products)
			})
		},

		// Add a new product
		addProduct({ state }, product) {
			return new Promise((resolve, reject) => {
				// Update object
				product = {
					// Default data
					id: new Date().getTime(),

					// Original data
					...product,
				}

				// Update state
				state.products.push(product)

				// Return product
				return resolve(product)
			})
		},
	}
}
