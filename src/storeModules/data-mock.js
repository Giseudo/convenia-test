export const tables = [
	{
		id: 1,
		name: 'Mesa 1',
		opened: null,
		debit: 0,
		orders: [],
		payments: []
	},
	{
		id: 2,
		name: 'Mesa 2',
		opened: null,
		debit: 0,
		orders: [],
		payments: []
	},
	{
		id: 2,
		name: 'Mesa 2',
		opened: null,
		debit: 0,
		orders: [],
		payments: []
	},
	{
		id: 3,
		name: 'Mesa 3',
		opened: null,
		debit: 0,
		orders: [],
		payments: []
	},
	{
		id: 4,
		name: 'Mesa 4',
		opened: null,
		debit: 0,
		orders: [],
		payments: []
	},
	{
		id: 5,
		name: 'Mesa 5',
		opened: null,
		debit: 0,
		orders: [],
		payments: []
	},
	{
		id: 6,
		name: 'Mesa 6',
		opened: null,
		debit: 0,
		orders: [],
		payments: []
	},
	{
		id: 7,
		name: 'Mesa 7',
		opened: null,
		debit: 0,
		orders: [],
		payments: []
	},
	{
		id: 8,
		name: 'Mesa 8',
		opened: null,
		debit: 0,
		orders: [],
		payments: []
	},
	{
		id: 9,
		name: 'Mesa 9',
		opened: null,
		debit: 0,
		orders: [],
		payments: []
	},
	{
		id: 10,
		name: 'Mesa 10',
		opened: null,
		debit: 0,
		orders: [],
		payments: []
	},
	{
		id: 11,
		name: 'Mesa 11',
		opened: null,
		debit: 0,
		orders: [],
		payments: []
	},
	{
		id: 12,
		name: 'Mesa 12',
		opened: null,
		debit: 0,
		orders: [],
		payments: []
	},
]

export const orders = [
	{
		id: 1,
		table: 1,
		product: 1,
		amount: 3,
		created_at: new Date()
	},
	{
		id: 2,
		table: 1,
		product: 2,
		amount: 1,
		created_at: new Date()
	},
	{
		id: 3,
		table: 2,
		product: 2,
		amount: 4,
		created_at: new Date()
	}
]

export const payments = [
	{
		id: 1,
		table: 1,
		value: 20.0,
		created_at: new Date(),
	},
	{
		id: 2,
		table: 1,
		value: 20.0,
		created_at: new Date(),
	}
]

export const products = [
	{
		id: 1,
		category: 'Bebidas',
		name: 'Cerveja Preta',
		price: 8.0
	},
	{
		id: 2,
		category: 'Bebidas',
		name: 'Taça de Vinho',
		price: 14.0
	},
	{
		id: 3,
		category: 'Bebidas',
		name: 'Caipirinha',
		price: 10.0
	},
	{
		id: 4,
		category: 'Bebidas',
		name: 'Whisky',
		price: 16.0
	},
	{
		id: 5,
		category: 'Petiscos',
		name: 'Batata Frita',
		price: 12.0
	},
	{
		id: 6,
		category: 'Petiscos',
		name: 'Calabresa Frita',
		price: 16.0
	},
	{
		id: 7,
		category: 'Petiscos',
		name: 'Pastel de Frango',
		price: 10.0
	},
	{
		id: 8,
		category: 'Refeições',
		name: 'Sanduiche Natural',
		price: 14.0
	},
	{
		id: 9,
		category: 'Refeições',
		name: 'Hamburger',
		price: 13.0
	},
	{
		id: 10,
		category: 'Saladas',
		name: 'Salada César',
		price: 18.0
	},
	{
		id: 11,
		category: 'Saladas',
		name: 'Legumes Refogados',
		price: 14.0
	},
]

export default {
	tables,	
	orders,
	payments,
	products
}
