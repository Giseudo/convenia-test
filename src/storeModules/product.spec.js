import Vue from 'vue'
import Vuex from 'vuex'
import { assert, expect } from 'chai'

import data from './data-mock'
import product from './product'

Vue.use(Vuex)

const	store = new Vuex.Store({ modules: { product } })
const	state = store.state.product

describe('products', () => {
	it('should load data', () =>
		store.dispatch('product/init', data.products)
			.then((products) => {
				expect(state.products.length)
					.to.equal(products.length)
			})
	)

	it('should add product', () =>
		store.dispatch('product/addProduct', {
			id: 12345,
			category: 'Comidas',
			name: 'Heineken 300ml',
			value: 15.0
		})
		.then(product => {
			assert.isObject(state.products.find(p => p.id == product.id))
		})
	)

	it('should find the product', () =>
		store.dispatch('product/getProduct', 12345)
		.then(product => {
			assert.isObject(product)
		})
	)

	it('should generate a random id', () =>
		store.dispatch('product/addProduct', {
			category: 'Bebidas',
			name: 'Heineken 300ml',
			value: 15.0
		})
		.then(product => {
			assert.isNotNull(product.id)
		})
	)
})
