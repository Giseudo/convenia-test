export default {
	namespaced: true,

	state: {
		orders: [],
	},

	getters: {
		getOrders(state) {
			return state.orders
		}
	},

	actions: {
		addOrders({ state, dispatch, rootGetters }, orders) {
			let created = []

			return new Promise((resolve, reject) => {
				for (let id in orders) {
					let amount = orders[id],
						// Find product
						product = rootGetters['product/find'](id),
						// Order object
						order = {
							// Default data
							id: new Date().getTime(),
							created_at: new Date(),

							// Original data
							...order,

							// Override data
							product: product,
							amount: amount,
							total: product.price * amount
						}

					// Add to state
					state.orders.push(order)

					// Add to the recent created order
					created.push(order)
				}

				return resolve(created)
			})
		},
	}
}
