import Vue from 'vue'
import Vuex from 'vuex'
import { assert, expect } from 'chai'

import data from './data-mock'
import product from './product'
import order from './order'

Vue.use(Vuex)

const	store = new Vuex.Store({ modules: { product, order } })
const state = store.state.order

// Init dependencies
store.dispatch('product/init', data.products)

describe('orders', () => {
	it('should load data', () =>
		store.dispatch('order/init', data.orders)
			.then(orders => {
				expect(state.orders.length)
					.to.equal(data.orders.length)
			})
	)

	it('should add order', () =>
		store.dispatch('order/addOrder', {
			id: 12345,
			table: 1,
			product: 1,
			amount: 3
		})
			.then(() => {
				expect(state.orders.length)
					.to.equal(data.orders.length + 1)
			})
	)

	it('should find the order', () =>
		store.dispatch('order/getOrder', 12345)
		.then(order => {
			assert.isObject(order)
		})
	)

	it('should generate a random id', () =>
		store.dispatch('order/addOrder', {
			table: 1,
			product: 1,
			amount: 1
		})
			.then(order => {
				assert.isNotNull(order.id)
			})
	)
})
