import Vue from 'vue'
import Vuex from 'vuex'
import { assert, expect } from 'chai'

import data from './data-mock'
import table from './table'

Vue.use(Vuex)

const store = new Vuex.Store({ modules: { table } })
const state = store.state.table

describe('tables', () => {
	it('should load data', () => {
		return store.dispatch('table/init', data.tables)
			.then(tables => {
				expect(state.tables.length)
					.to.equal(data.tables.length)
			})
	})

	it('should add table', () =>
		store.dispatch('table/addTable', {
			id: 12345,
			name: 'Mesa 30',
			checkin: new Date(),
			orders: [],
			payments: []
		})
		.then(table => {
			expect(state.tables.length)
				.to.equal(data.tables.length + 1)
		})
	)

	it('should find the table', () =>
		store.dispatch('table/getTable', 12345)
		.then(table => {
			assert.isObject(table)
		})
	)

	it('should generate a random id', () =>
		store.dispatch('table/addTable', {
			name: 'Mesa 30',
			checkin: new Date(),
		})
		.then(table => {
			assert.isNotNull(table.id)
			assert.isNotNull(table.orders)
			assert.isNotNull(table.payments)
		})
	)
})
