import moment from 'moment'

export default (value) => {
	return moment(value).format('DD/MM/YYYY HH:mm')
}
