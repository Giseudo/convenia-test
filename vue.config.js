const webpack = require('webpack')
const path = require('path')

module.exports = {
	css: {
		loaderOptions: {
			sass: {
				data: "@import '~@/stylesheets/core.scss';",
				includePaths: [
					require("bourbon-neat").includePaths,
				]
			}
		}
	},
	configureWebpack: {
		plugins: [
			new webpack.DefinePlugin({
				'process.env': {
					// 'API_URL': JSON.stringify(process.env.NODE_ENV == 'development' ? 'http://localhost:8080' : process.env.API_URL),
				}
			})
		]
	},
	chainWebpack: (config) => {
		config.module
			.rule('storysource')
			.test(/\.stories\.js?$/)
			.post()
			.use('storysource')
			.loader(require.resolve('@storybook/addon-storysource/loader'))
	}
}
